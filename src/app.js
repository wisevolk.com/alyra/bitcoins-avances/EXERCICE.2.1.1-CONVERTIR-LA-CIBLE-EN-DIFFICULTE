const readline = require('readline');
const chalk = require("chalk");



const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: false
});

const calculCible = (cible) => {
    const TARGET_MAX = (Math.pow(2, 16) - 1) * Math.pow(2, 208);
    return TARGET_MAX / cible;
}

rl.setPrompt("Entrez votre cible");
rl.on("line", input => {
    console.log(calculCible(input));
})
rl.prompt();
